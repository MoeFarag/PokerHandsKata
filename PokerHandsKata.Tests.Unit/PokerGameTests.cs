﻿using PokerHandsKata.BusinessLogic;
using NUnit.Framework;
using FluentAssertions;

namespace PokerHandsKata.Tests.Unit
{
    [TestFixture]
    public class PokerGameTests
    {
        private IPokerGame _game;

        [SetUp]
        public void SetUp()
        {
            var hand1Name = "Hand1";
            var hand2Name = "Hand2";
            _game = new PokerGame(new StandardPokerRules(hand1Name, hand2Name));
        }

        #region Tie Tests

        [Test]
        public void RankMethod_TiedHands_ReturnsTie()
        {
            //Arrange
            var hand1 = "2H 3D 5S 9C KD";
            var hand2 = "2D 3H 5C 9S KH";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Tie");
        }

        [Test]
        public void RankMethod_TiedStraightFlushHands_ReturnsTie()
        {
            //Arrange
            var hand1 = "AS TS JS KS QS";
            var hand2 = "TC QC JC KC AC";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Tie");
        }

        #endregion

        #region High Card Tests

        [Test]
        public void RankMethod_2HandsWithHighCard_ReturnsWinnerAndHighCard()
        {
            //Arrange
            var hand1 = "2H 3D 5S 9C KD";
            var hand2 = "2C 3H 4S 8C AH";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - High Card: A");
        }

        [Test]
        public void RankMethod_2HandsWithSameHighCard_ReturnsWinnerAndNextHighCard()
        {
            //Arrange
            var hand1 = "2H 3D 5S 9C AD";
            var hand2 = "2C 3H 4S 8C AH";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - High Card: 9");
        }

        #endregion

        #region Pair Tests

        [Test]
        public void RankMethod_2HandsWithOnePair_ReturnsWinnerAndPairRank()
        {
            //Arrange
            var hand1 = "2H 3D 5S 9C AD";
            var hand2 = "2C 3H AH 8C 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - Pair: 8");
        }

        [Test]
        public void RankMethod_2HandsWithSamePairInEach_ReturnsWinnerAndHighCard()
        {
            //Arrange
            var hand1 = "2H 3D 5S 8C 8S";
            var hand2 = "2C 3H 4S 8D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - Pair: 5");
        }

        [Test]
        public void RankMethod_2HandsWithDifferentPairInEach_ReturnsWinnerAndPairRank()
        {
            //Arrange
            var hand1 = "2H 3D 5S 8C 8S";
            var hand2 = "2C 3H 4S 9D 9H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - Pair: 9");
        }

        #endregion

        #region Two Pair Tests

        [Test]
        public void RankMethod_2HandsWith2PairInOneHand_ReturnsWinnerAndPairRank()
        {
            //Arrange
            var hand1 = "2H 5D 5S 8C 8S";
            var hand2 = "2C 3H 4S 7D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - Two Pair: 8, 5");
        }

        [Test]
        public void RankMethod_2HandsWith2PairAnd1Pair_ReturnsWinnerAndPairRank()
        {
            //Arrange
            var hand1 = "2H 5D 5S 8C 8S";
            var hand2 = "2C 3H 4S 8D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - Two Pair: 8, 5");
        }

        [Test]
        public void RankMethod_2HandsWith2PairsInEach_ReturnsWinnerAndPairRank()
        {
            //Arrange
            var hand1 = "2H 5D 5S 8C 8S";
            var hand2 = "2C 4H 4S 8D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - Two Pair: 5");
        }

        [Test]
        public void RankMethod_2HandsWithSame2PairsInEach_ReturnsWinnerAndHighCard()
        {
            //Arrange
            var hand1 = "2H 5D 5S 8C 8S";
            var hand2 = "3C 5H 5C 8D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - Two Pair: 3");
        }
        #endregion

        #region 3 of a Kind Tests

        [Test]
        public void RankMethod_1HandWith3OfAKind_ReturnsWinnerAndRank()
        {
            //Arrange
            var hand1 = "TH 5D TD 3S TS";
            var hand2 = "3C 5H 5C 6D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - 3 of a Kind: T");
        }

        [Test]
        public void RankMethod_2HandsWith3OfAKind_ReturnsWinnerAndRank()
        {
            //Arrange
            var hand1 = "TH 5D TD 8C TS";
            var hand2 = "5C QH 3C QD QH";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - 3 of a Kind: Q");
        }

        #endregion

        #region Straight

        [Test]
        public void RankMethod_1HandWithStraight_ReturnsWinnerAndRank()
        {
            //Arrange
            var hand1 = "9H TD JD KS QS";
            var hand2 = "3C 5H 5C 6D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - Straight: K, Q, J, T, 9");
        }

        [Test]
        public void RankMethod_2HandsWithStraight_ReturnsWinnerAndRank()
        {
            //Arrange
            var hand1 = "9H TD JD KS QS";
            var hand2 = "TC QH JC KD AH";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - Straight: A");
        }

        #endregion

        #region Flush

        [Test]
        public void RankMethod_1HandWithFlush_ReturnsWinnerAndRank()
        {
            //Arrange
            var hand1 = "9H 2H 8H 5H QH";
            var hand2 = "3C 5H 2C 6D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - Flush: Q, 9, 8, 5, 2");
        }

        [Test]
        public void RankMethod_2HandsWithFlush_ReturnsWinnerAndHighCard()
        {
            //Arrange
            var hand1 = "9H 2H 8H 5H QH";
            var hand2 = "3C 5C 2C 6C AC";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - Flush: A");
        }

        #endregion

        #region Full House

        [Test]
        public void RankMethod_1HandWithFullHouse_ReturnsWinnerAndRank()
        {
            //Arrange
            var hand1 = "9H 9C 9D 5D 5C";
            var hand2 = "3C 5H 2C 6D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - Full House: 9, 5");
        }

        [Test]
        public void RankMethod_2HandsWithFullHouse_ReturnsWinnerAndHighCard()
        {
            //Arrange
            var hand1 = "9H 9C 9D 5D 5C";
            var hand2 = "TC TD TH 6C 6H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - Full House: T, 6");
        }

        #endregion

        #region 4 of a Kind

        [Test]
        public void RankMethod_1HandWith4OfAKind_ReturnsWinnerAndRank()
        {
            //Arrange
            var hand1 = "9H 9C 9D 9S 5C";
            var hand2 = "3C 5H 2C 6D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - 4 of a Kind: 9");
        }

        [Test]
        public void RankMethod_2HandsWith4OfAKind_ReturnsWinnerAndHighCard()
        {
            //Arrange
            var hand1 = "9H 9C 9D 9S 5C";
            var hand2 = "TC TD TH TS 6H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - 4 of a Kind: T");
        }

        #endregion

        #region Straight Flush

        [Test]
        public void RankMethod_1HandWithStraightFlush_ReturnsWinnerAndRank()
        {
            //Arrange
            var hand1 = "9H TH JH KH QH";
            var hand2 = "3C 5H 5C 6D 8H";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand1 wins - Straight Flush: K, Q, J, T, 9");
        }

        [Test]
        public void RankMethod_2HandsWithStraightFlush_ReturnsWinnerAndRank()
        {
            //Arrange
            var hand1 = "9S TS JS KS QS";
            var hand2 = "TC QC JC KC AC";

            //Action
            var result = _game.Rank(hand1, hand2);

            //Assert
            result.Should().Be("Hand2 wins - Straight Flush: A");
        }

        #endregion

    }
}
