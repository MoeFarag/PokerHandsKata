﻿namespace PokerHandsKata.BusinessLogic
{
    public interface IPokerGame
    {
        string Rank(string hand1, string hand2);
    }
}
