﻿using System.Collections.Generic;

namespace PokerHandsKata.BusinessLogic
{
    public interface IPokerRankersSource
    {
        IDictionary<string, Ranker> Rankers { get; }
    }
}
