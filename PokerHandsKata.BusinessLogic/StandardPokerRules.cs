﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PokerHandsKata.BusinessLogic
{
    public class StandardPokerRules : IPokerRankersSource
    {
        private readonly string _hand1;
        private readonly string _hand2;

        public IDictionary<string, Ranker> Rankers => GetRules();

        public StandardPokerRules(string hand1Name, string hand2Name)
        {
            _hand1 = hand1Name;
            _hand2 = hand2Name;
        }

        private IDictionary<string, Ranker> GetRules()
        {
            var rankers = new Dictionary<int, Tuple<string,Ranker>>();

            rankers.Add(0, new Tuple<string, Ranker>("High Card", TryHighCard));
            rankers.Add(1, new Tuple<string, Ranker>("Pair", TryPair));
            rankers.Add(2, new Tuple<string, Ranker>("Two Pair", TryTwoPair));
            rankers.Add(3, new Tuple<string, Ranker>("3 of a Kind", Try3OfAKind));
            rankers.Add(4, new Tuple<string, Ranker>("Straight", TryStraight));
            rankers.Add(5, new Tuple<string, Ranker>("Flush", TryFlush));
            rankers.Add(6, new Tuple<string, Ranker>("Full House", TryFullHouse));
            rankers.Add(7, new Tuple<string, Ranker>("4 of a Kind", Try4OfAKind));
            rankers.Add(8, new Tuple<string, Ranker>("Straight Flush", TryStraightFlush));

            return rankers.Reverse().ToDictionary(kvp=> kvp.Value.Item1, kvp=> kvp.Value.Item2);
        }

        private bool DecideWinner(string winner, Card[] winningCards, out string actualWinner, out Card[] actualWinningCards)
        {
            actualWinner = winner;
            actualWinningCards = winningCards;
            return (winningCards?.Any() ?? false) && winner != null;
        }

        private IGrouping<int, Card>[] GetGroupsOf(int pairCount, int groupCount, Card[] hand)
        {
            var grps = hand.GroupBy(c => c.ValueOfCard)
            .Where(cards => cards.Count() == pairCount)
            .OrderByDescending(grp => grp.Key).ToArray();

            return grps.Length == groupCount ? grps : new IGrouping < int, Card >[] {};
        }

        private bool IsStraight(Card[] hand)
        {
            var sortedHand = hand.OrderByDescending(c => c.ValueOfCard).ToArray();

            for (int i = 0; i < sortedHand.Length-1; i++)
            {
                if (sortedHand[i].ValueOfCard - sortedHand[i + 1].ValueOfCard > 1)
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsFlush(Card[] hand)
        {
            var suit = hand[0].Suit;
            return hand.Count(c => c.Suit == suit) == hand.Length;
        }

        private bool TryStraightFlush(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards)
        {
            var hand1IsStraightFlush = IsFlush(hand1) && IsStraight(hand1);
            var hand2IsStraightFlush = IsFlush(hand2) && IsStraight(hand2);

            if (hand1IsStraightFlush && !hand2IsStraightFlush)
            {
                return DecideWinner(_hand1, hand1, out winner, out winningCards);
            }
            if (!hand1IsStraightFlush && hand2IsStraightFlush)
            {
                return DecideWinner(_hand2, hand2, out winner, out winningCards);
            }
            if (hand1IsStraightFlush && hand2IsStraightFlush)
            {
                return TryHighCard(hand1, hand2, out winner, out winningCards);
            }

            return DecideWinner("", null, out winner, out winningCards);
        }

        private bool Try4OfAKind(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards)
        {
            var hand14OfAKind = GetGroupsOf(4, 1, hand1);
            var hand24OfAKind = GetGroupsOf(4, 1, hand2);

            if (hand14OfAKind.Any() && !hand24OfAKind.Any())
            {
                return DecideWinner(_hand1, hand14OfAKind.Select(grp => grp.First()).ToArray(), out winner, out winningCards);
            }

            if (!hand14OfAKind.Any() && hand24OfAKind.Any())
            {
                return DecideWinner(_hand2, hand24OfAKind.Select(grp => grp.First()).ToArray(), out winner, out winningCards);
            }
            if (hand14OfAKind.Any() && hand24OfAKind.Any())
            {
                return TryHighCard(hand14OfAKind.First().ToArray(), hand24OfAKind.First().ToArray(), out winner, out winningCards);
            }

            return DecideWinner("", null, out winner, out winningCards);
        }

        private bool TryFullHouse(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards)
        {
            if (Try3OfAKind(hand1, hand2, out winner, out winningCards))
            {
                if(winner == _hand1)
                { 
                        Card[] winningPair;
                        if (TryPair(hand1, new Card[0], out winner, out winningPair))
                        {
                            return DecideWinner(_hand1, winningCards.Concat(winningPair).ToArray(), out winner, out winningCards);
                        }
                }

                if (winner == _hand2)
                {
                    Card[] winningPair;
                    if (TryPair(new Card[0], hand2, out winner, out winningPair))
                    {
                        return DecideWinner(_hand2, winningCards.Concat(winningPair).ToArray(), out winner, out winningCards);
                    }
                }
            }
            return DecideWinner("", null, out winner, out winningCards);
        }

        private bool TryFlush(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards)
        {
            var hand1IsFlush = IsFlush(hand1);
            var hand2IsFlush = IsFlush(hand2);

            if (hand1IsFlush && !hand2IsFlush)
            {
                return DecideWinner(_hand1, hand1, out winner, out winningCards);
            }
            if (!hand1IsFlush && hand2IsFlush)
            {
                return DecideWinner(_hand2, hand2, out winner, out winningCards);
            }
            if (hand1IsFlush && hand2IsFlush)
            {
                return TryHighCard(hand1, hand2, out winner, out winningCards);
            }

            return DecideWinner("", null, out winner, out winningCards);
        }

        private bool TryStraight(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards)
        {
            var hand1IsStraight = IsStraight(hand1);
            var hand2IsStraight = IsStraight(hand2);

            if (hand1IsStraight && !hand2IsStraight)
            {
                return DecideWinner(_hand1, hand1 , out winner, out winningCards);
            }
            if (!hand1IsStraight && hand2IsStraight)
            {
                return DecideWinner(_hand2, hand2, out winner, out winningCards);
            }
            if (hand1IsStraight && hand2IsStraight)
            {
                return TryHighCard(hand1, hand2, out winner, out winningCards);
            }

            return DecideWinner("", null, out winner, out winningCards);
        }

        private bool Try3OfAKind(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards)
        {
            var hand13OfAKind = GetGroupsOf(3, 1, hand1);
            var hand23OfAKind = GetGroupsOf(3, 1, hand2);

            if (hand13OfAKind.Any() && !hand23OfAKind.Any())
            {
                return DecideWinner(_hand1, hand13OfAKind.Select(grp => grp.First()).ToArray(), out winner, out winningCards);
            }

            if (!hand13OfAKind.Any() && hand23OfAKind.Any())
            {
                return DecideWinner(_hand2, hand23OfAKind.Select(grp => grp.First()).ToArray(), out winner, out winningCards);
            }
            if (hand13OfAKind.Any() && hand23OfAKind.Any())
            {
                return TryHighCard(hand13OfAKind.First().ToArray(), hand23OfAKind.First().ToArray(), out winner, out winningCards);
            }

            return DecideWinner("", null, out winner, out winningCards);
        }

        private bool TryTwoPair(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards)
        {
            var hand1Pairs = GetGroupsOf(2, 2, hand1);
            var hand2Pairs = GetGroupsOf(2, 2, hand2);

            if (hand1Pairs.Any() && !hand2Pairs.Any())
            {
                return DecideWinner(_hand1, hand1Pairs.Select(grp => grp.First()).ToArray(), out winner, out winningCards);
            }

            if (!hand1Pairs.Any() && hand2Pairs.Any())
            {
                return DecideWinner(_hand2, hand2Pairs.Select(grp => grp.First()).ToArray(), out winner, out winningCards);
            }
            if (hand1Pairs.Any() && hand2Pairs.Any())
            {
                return TryHighCard(hand1, hand2, out winner, out winningCards);
            }

           return DecideWinner("",null, out winner, out winningCards);
        }

        private bool TryPair(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards)
        {
            var hand1PairedCard = GetGroupsOf(2, 1, hand1).FirstOrDefault()?.FirstOrDefault();
            var hand2PairedCard = GetGroupsOf(2, 1, hand2).FirstOrDefault()?.FirstOrDefault();

            if (hand1PairedCard != null && hand2PairedCard == null)
            {
                return DecideWinner(_hand1, new[] { hand1PairedCard}, out winner, out winningCards);
            }

            if (hand1PairedCard == null && hand2PairedCard != null)
            {
                return DecideWinner(_hand2, new[] { hand2PairedCard }, out winner, out winningCards);
            }

            if (hand1PairedCard != null && hand2PairedCard != null)
            {
                return TryHighCard(hand1, hand2, out winner, out winningCards);
            }
            return DecideWinner("",null, out winner, out winningCards);
        }

        private bool TryHighCard(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards)
        {
            if (!hand1.Any() || !hand2.Any())
            {
                return DecideWinner("", null, out winner, out winningCards);
            }

            var hand1MaxValue = hand1.Max(c => c.ValueOfCard);
            var hand2MaxValue = hand2.Max(c => c.ValueOfCard);

            if (hand1MaxValue > hand2MaxValue)
            {
                return DecideWinner(_hand1, new[] { hand1[0]}, out winner, out winningCards);
            }

            if (hand1MaxValue < hand2MaxValue)
            {
                return DecideWinner(_hand2, new [] { hand2[0]}, out winner, out winningCards);
            }

            return TryHighCard(hand1.Skip(1).ToArray(), hand2.Skip(1).ToArray(), out winner, out winningCards);
        }
    }


}
