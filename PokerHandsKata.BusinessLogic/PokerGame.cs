﻿using System.Collections.Generic;
using System.Linq;

namespace PokerHandsKata.BusinessLogic
{
    public delegate bool Ranker(Card[] hand1, Card[] hand2, out string winner, out Card[] winningCards);

    public class PokerGame : IPokerGame
    {
        private readonly IDictionary<string, Ranker> _rankers;

        public PokerGame(IPokerRankersSource rankersSource)
        {
            _rankers = rankersSource.Rankers;
        }

        public string Rank(string hand1, string hand2)
        {
            var hand1Cards = GetCards(hand1);
            var hand2Cards = GetCards(hand2);
            string winner="";
            string rank = "";
            Card[] winningCards = null;

            foreach (var kvp  in _rankers)
            {
                if(kvp.Value(hand1Cards,hand2Cards,out winner, out winningCards))
                {
                    rank = kvp.Key;
                    break;
                }
            }

            return WinningMessage(winner, rank, winningCards);
        }

        private Card[] GetCards(string hand)
        {
            return hand.Split(' ').Select(c => new Card(c)).OrderByDescending(c => c.ValueOfCard).ToArray();
        }

        private string WinningMessage(string winner, string rank, Card[] winningCards)
        {
            return winner == ""? "Tie": $"{winner} wins - {rank}: {string.Join(", ", winningCards.Select(c => c.Number))}";
        }
    }
}
