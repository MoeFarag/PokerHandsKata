﻿using System;
using System.Collections.Generic;

namespace PokerHandsKata.BusinessLogic
{
    public class Card
    {
        private static readonly List<string> CardNumbers;
        public string Number { get; }
        public string Suit { get; }
        public int ValueOfCard => CardNumbers.IndexOf(Number);

        static Card()
        {
            CardNumbers =  new List<string>{ "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"};
        }

        public static bool operator > (Card card1, Card card2)
        {
            return card1.ValueOfCard > card2.ValueOfCard;
        }

        public static bool operator < (Card card1, Card card2)
        {
            return card1.ValueOfCard < card2.ValueOfCard;
        }

        public Card(string card)
        {
            Number = card.Substring(0, 1);
            Suit = card.Substring(1, 1);
        }

        public override string ToString()
        {
            return Number + Suit;
        }

        public override bool Equals(object obj)
        {
            var otherCard = obj as Card;
            if(otherCard != null)
                return otherCard.Number == Number && otherCard.Suit == Suit;
            throw new NotSupportedException("Card Expected");
        }

        public override int GetHashCode()
        {
            return Number.GetHashCode() + Suit.GetHashCode();
        }
    }
}
